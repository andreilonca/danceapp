drop database dance;
create database Dance;
use Dance;
CREATE TABLE `dance`.`event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  `country` VARCHAR(45) NOT NULL,
  `date` VARCHAR(30) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
  INSERT INTO `dance`.`event` (`name`, `location`, `country`, `date`) VALUES ('WDSF BUDAPEST', 'BUDAPEST', 'HUNGARY', '10.09.2018');
INSERT INTO `dance`.`event` (`name`, `location`, `country`, `date`) VALUES ('WDSF BELGRAD', 'BELGRAD', 'SERBIA', '07.07.2018');
select * from event;
CREATE TABLE `dance`.`event_detail` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number_of_couples` INT NULL,
  `id_event` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `id_event_fk_idx` (`id_event` ASC),
  CONSTRAINT `id_event_fk`
  FOREIGN KEY (`id_event`)
  REFERENCES `dance`.`event` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION);
INSERT INTO `dance`.`event_detail` (`number_of_couples`, `id_event`) VALUES ('100', '1');   
INSERT INTO `dance`.`event_detail` (`number_of_couples`, `id_event`) VALUES ('250', '2');   
select * from event_detail;

select country from Event where id=(select id_event from event_detail where id=1);
CREATE TABLE `dance`.`dancer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `laste_name` VARCHAR(45) NULL,
  `age` INT NULL,
  `country` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
ALTER TABLE `dance`.`dancer` 
ADD COLUMN `couple_id` INT NULL AFTER `country`;
ALTER TABLE `dance`.`dancer` 
ADD COLUMN `id_event_detail` INT NULL AFTER `couple_id`;
 
INSERT INTO `dance`.`dancer` (`first_name`, `laste_name`, `age`, `country`) VALUES ('andrei', 'lonca', '16', 'romania');
INSERT INTO `dance`.`dancer` (`first_name`, `laste_name`, `age`, `country`) VALUES ('daria', 'mesesan', '16', 'romania');
INSERT INTO `dance`.`dancer` (`first_name`, `laste_name`, `age`, `country`) VALUES ('dragos', 'pop', '14', 'ramania');
INSERT INTO `dance`.`dancer` (`first_name`, `laste_name`, `age`, `country`) VALUES ('elena', 'popescu', '15', 'romania');
INSERT INTO `dance`.`dancer` (`first_name`, `laste_name`, `age`, `country`) VALUES ('raul', 'zirbo', '15', 'romania');
INSERT INTO `dance`.`dancer` (`first_name`, `laste_name`, `age`, `country`) VALUES ('oana', 'saca', '14', 'romania');
 select*from dancer;
UPDATE `dance`.`dancer` SET `id_event_detail`='1' WHERE `id`='1';
UPDATE `dance`.`dancer` SET `id_event_detail`='1' WHERE `id`='2';
UPDATE `dance`.`dancer` SET `id_event_detail`='1' WHERE `id`='3';
UPDATE `dance`.`dancer` SET `id_event_detail`='1' WHERE `id`='4';
UPDATE `dance`.`dancer` SET `id_event_detail`='1' WHERE `id`='5';
UPDATE `dance`.`dancer` SET `id_event_detail`='1' WHERE `id`='6';


  CREATE TABLE `dance`.`couple` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_male` INT NULL,
  `id_female` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_male_idx` (`id_male` ASC),
  INDEX `id_female_idx` (`id_female` ASC),
  CONSTRAINT `id_male`
  FOREIGN KEY (`id_male`)
  REFERENCES `dance`.`dancer` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `id_female`
  FOREIGN KEY (`id_female`)
  REFERENCES `dance`.`dancer` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION);

INSERT INTO `dance`.`couple` (`id_male`, `id_female`) VALUES ('1', '2');
INSERT INTO `dance`.`couple` (`id_male`, `id_female`) VALUES ('3', '4');
INSERT INTO `dance`.`couple` (`id_male`, `id_female`) VALUES ('5', '6');
select*from couple;
CREATE TABLE `dance`.`event_detail_couple` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_couple` INT NULL,
  `id_event_detail` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_couple_idx` (`id_couple` ASC),
  INDEX `id_event_detail_idx` (`id_event_detail` ASC),
  CONSTRAINT `id_couple`
  FOREIGN KEY (`id_couple`)
  REFERENCES `dance`.`couple` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `id_event_detail`
  FOREIGN KEY (`id_event_detail`)
  REFERENCES `dance`.`event_detail` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION);
  INSERT INTO `dance`.`event_detail_couple` (`id_couple`, `id_event_detail`) VALUES ('1', '1');
INSERT INTO `dance`.`event_detail_couple` (`id_couple`, `id_event_detail`) VALUES ('2', '2');
INSERT INTO `dance`.`event_detail_couple` (`id_couple`, `id_event_detail`) VALUES ('3', '2');

select * from event_detail_couple;
CREATE TABLE `dance`.`referee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `age` INT NULL,
  `country` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  select*from referee;
  INSERT INTO `dance`.`referee` (`first_name`, `last_name`, `age`, `country`) VALUES ('ioana', 'damsa', '23', 'romania');
INSERT INTO `dance`.`referee` (`first_name`, `last_name`, `age`, `country`) VALUES ('alexey', 'gozun', '38', 'ukraine');

  CREATE TABLE `dance`.`event_detail_referee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_event_detail1` INT NULL,
  `id_referee` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_event_detail_idx` (`id_event_detail1` ASC),
  INDEX `id_referee_idx` (`id_referee` ASC),
  CONSTRAINT `id_event_detail1`
  FOREIGN KEY (`id_event_detail1`)
  REFERENCES `dance`.`event_detail` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `id_referee`
  FOREIGN KEY (`id_referee`)
  REFERENCES `dance`.`referee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION);
INSERT INTO `dance`.`event_detail_referee` (`id_event_detail1`, `id_referee`) VALUES ('1', '1');
INSERT INTO `dance`.`event_detail_referee` (`id_event_detail1`, `id_referee`) VALUES ('2', '2');

select*from event_detail_referee;
select * from dancer;
-- pasul 1
select * from couple where id=1;
-- pasul 2
select * from couple where id=(select id_couple from event_detail_couple where id =1);
-- pasul 3
select * from couple where id=(select id_event_detail from event_detail_couple where id =1);
-- pasul 4
select * from dancer;
select count(*) from couple;
select count(*) from dancer;
select count(*) from referee;
select count(*) from event;
select id from Event_Detail_Couple where id_couple=(select id from Couple where id_male =1);

select id,first_name, laste_name from Dancer where id_event_detail = (
	select id from event_detail where id_event = 1)
;


