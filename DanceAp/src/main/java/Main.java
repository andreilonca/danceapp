import db.*;
import model.*;
import ui.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Andrei Lonca
 * Date: 20.04.2018
 * Time: 17:15
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public static void main(String args[]) {

        DancersProvider dancersProvider = new DancersProvider(JDBCConnection.getConnection());
        List<Dancer> dancers = dancersProvider.provideDancers();
        for (Dancer dancer : dancers) {
            System.out.println(dancer.toString());
        }
        RefereesProvider refereesProvider = new RefereesProvider(JDBCConnection.getConnection());
        List<Referee> referees = refereesProvider.provideReferees();
        for (Referee referee : referees) {
            System.out.println(referee.toString());
        }
        EventsProvider eventsProvider = new EventsProvider(JDBCConnection.getConnection());
        List<Event> events = eventsProvider.provideEvents();
        for (Event event : events) {
            System.out.println(event.toString());
        }

        CouplesProvider couplesProvider = new CouplesProvider(JDBCConnection.getConnection());
        List<Couple> couples = couplesProvider.provideCouples();
        for (Couple couple : couples) {
            System.out.println(couple.toString());
        }
        NewsProvider newsProvider = new NewsProvider(JDBCConnection.getConnection());
        List<News> news = newsProvider.provideNews();
        for (News news1 : news) {
            System.out.println(news1.toString());
        }
        new AppView();
        System.out.println(couplesProvider.getNumberOfCouples());
        couplesProvider.provideCouples(1);
    }
}
