package db;

import model.Couple;
import model.model_view.CoupleView;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class CouplesProvider {

    private Connection connection;
    private static final String SELECT_QUERY = " select * from couple" ;
    private static final String SELECT_NUMBEROFCOUPLES = "select count(*) from couple" ;
    private static final String SELECT_COUPLEVIEWLIST = "select id,first_name, laste_name from Dancer where id_event_detail = (\n" +
            "\tselect id from event_detail where id_event = ";
    public CouplesProvider(Connection connection) {
        this.connection = connection;

    }

    public List<Couple> provideCouples() {
        List<Couple> couples = new ArrayList<Couple>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_QUERY);
            while (resultSet.next()) {
                Couple couple = new Couple();
                int id = resultSet.getInt("id");
                int id_male = resultSet.getInt("id_male");
                int id_female = resultSet.getInt("id_female");
                couple.setId(id);
                couple.setId_male(id_male);
                couple.setId_male(id_male);
                couples.add(couple);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return couples;
    }
    public int getNumberOfCouples(){
       int count = -1;
        try {
           Statement statement = connection.createStatement();
           ResultSet resultSet = statement.executeQuery(SELECT_NUMBEROFCOUPLES);
           if(resultSet.next()){
                count = resultSet.getInt("count(*)");
           }
       }
       catch (SQLException e)
       {
           System.out.println(e.getMessage());
           return -1;
           }
        return count;
    }
    public List<CoupleView> provideCouples(int eventId){
        List<CoupleView> couplesview = new ArrayList<CoupleView>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_COUPLEVIEWLIST+ eventId +")");
            while (resultSet.next()) {
                CoupleView coupleview = new CoupleView();
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("laste_name");
                coupleview.setId(id);
                coupleview.setFirstName(firstName);
                coupleview.setLastName(lastName);
                couplesview.add(coupleview);
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return couplesview;
    }
}
