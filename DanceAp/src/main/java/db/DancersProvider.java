package db;


import model.Dancer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Andrei Lonca
 * Date: 20.04.2018
 * Time: 17:33
 * To change this template use File | Settings | File Templates.
 */
public class DancersProvider {

    private Connection connection;
    private static final String SELECT_QUERY = " select * from dancer ";
    private static final String SELECT_ALL = "select * from dancer where id = ";

    public DancersProvider(Connection connection) {
        this.connection = connection;

    }

    public List<Dancer> provideDancers() {
        List<Dancer> dancers = new ArrayList<Dancer>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_QUERY);
            while (resultSet.next()) {
                dancers.add(parseDancer(resultSet));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return dancers;
    }

    public Dancer getDancerByID(int id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL + id);
            if (resultSet.next()) {
                return parseDancer(resultSet);
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    private Dancer parseDancer(ResultSet resultSet) {
        try {
            Dancer dancer = new Dancer();
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("laste_name");
            int age = resultSet.getInt("age");
            String country = resultSet.getString("country");
            String info = resultSet.getString("info");
            String image = resultSet.getString("image");
            dancer.setId(id);
            dancer.setFirstName(firstName);
            dancer.setLastName(lastName);
            dancer.setAge(age);
            dancer.setCountry(country);
            dancer.setInfo(info);
            dancer.setImage(image);
            return dancer;
        } catch (SQLException e) {
            return null;
        }
    }

}
