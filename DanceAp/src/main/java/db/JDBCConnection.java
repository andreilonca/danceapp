package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Andrei Lonca
 * Date: 20.04.2018
 * Time: 17:32
 * To change this template use File | Settings | File Templates.
 */
public class JDBCConnection {
    public static Connection getConnection() {
        System.out.println("--------- MySQL JDBC Connection Testing ----------");
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your mySQl JDBC Driver?");
            return null;
        }
        System.out.println("MySQL JDBC Driver Registered!");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dance", "root", "");
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            return null;
        }
        if (connection != null) {
            System.out.println("You made it, take control your datbase now");
            return connection;
        } else {
            System.out.println("Failed to make connections");
            return null;
        }
    }
}
