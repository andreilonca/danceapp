package db;
import model.Event;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Andrei Lonca
 * Date: 21.04.2018
 * Time: 12:25
 * To change this template use File | Settings | File Templates.
 */
public class EventsProvider {

    private Connection connection;
    private static final String SELECT_QUERY = " select * from event";
    private static final String INSERT_QUERY = " INSERT INTO `dance`.`event` (`name`, `location`, `country`, `date`) VALUES('";
    public EventsProvider(Connection connection) {
        this.connection = connection;
    }
    List<Event> events = new ArrayList<Event>();
    public List<Event> provideEvents() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_QUERY);
            while (resultSet.next()) {
                Event event = new Event();
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String location= resultSet.getString("location");
                String country = resultSet.getString("country");
                String date = resultSet.getString("date");
                event.setId(id);
                event.setName(name);
                event.setLocation(location);
                event.setCountry(country);
                event.setDate(date);
                events.add(event);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return events;
    }

    public void insertEvent(Event event) {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(INSERT_QUERY+event.getName()+"','"+event.getLocation()+
            "','"+event.getCountry()+"','"+event.getDate()+"');");

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }
}
