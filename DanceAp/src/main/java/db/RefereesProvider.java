package db;

import model.Dancer;
import model.Referee;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Andrei Lonca
 * Date: 21.04.2018
 * Time: 12:09
 * To change this template use File | Settings | File Templates.
 */
public class RefereesProvider {

    private Connection connection;
    private static final String SELECT_QUERY = " select * from referee ";

    public RefereesProvider(Connection connection) {
        this.connection = connection;
    }
    public List<Referee> provideReferees() {
        List<Referee> referees = new ArrayList<Referee>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_QUERY);
            while (resultSet.next()) {
                Referee referee = new Referee();
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                int age = resultSet.getInt("age");
                String country = resultSet.getString("country");
                referee.setId(id);
                referee.setFirstName(firstName);
                referee.setLastName(lastName);
                referee.setAge(age);
                referee.setCountry(country);
                referees.add(referee);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
        return referees;
    }


}
