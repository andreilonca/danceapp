package db;

import model.News;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class NewsProvider {
    private Connection connection;
    private static final String SELECT_QUERY = " select * from news ";
    public NewsProvider(Connection conection){this.connection = conection;}
    public List<News> provideNews(){
        List<News>news = new ArrayList<News>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_QUERY);
            while (resultSet.next()){
                News news1 = new News();
                int id = resultSet.getInt("id");
                String text = resultSet.getString("text");
                String image = resultSet.getString("image");
                news1.setId(id);
                news1.setText(text);
                news1.setImage(image);
                news.add(news1);
            }
        } catch (SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
        return news;
    }
}
