package listeners;

public interface ItemSelectedListener {
    void onItemClicked(String itemId);
}
