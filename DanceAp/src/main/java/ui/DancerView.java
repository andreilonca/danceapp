package ui;

import model.Dancer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class DancerView extends JFrame {
    private static final int WIDTH_APP = 400;
    private static final int HEIGHT_APP = 380;
    JFrame frame = new JFrame();
    JPanel panelMain = new JPanel();
    JPanel panelRight;
    Dancer dancer;
    public DancerView(Dancer dancer) {
        this.dancer= dancer;
        setBackground(new Color(48, 73, 155));
        frame.setSize(WIDTH_APP+20, HEIGHT_APP);
        frame.setTitle("Dancer");
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        GridBagConstraints gridBagConstrains = new GridBagConstraints();
        gridBagConstrains.anchor = GridBagConstraints.EAST;

        JPanel panelLeft = new JPanel(new GridBagLayout());
        panelLeft.setBackground(new Color(137, 255, 228));
        panelLeft.setPreferredSize(new Dimension(WIDTH_APP/3,HEIGHT_APP));
        panelRight = new JPanel(new GridBagLayout());
        panelRight.setBackground(new Color(85, 155, 132));
        panelRight.setPreferredSize(new Dimension((WIDTH_APP*2)/3, HEIGHT_APP));

        JLabel firstName = new JLabel("First Name");
        JLabel dancerName = new JLabel(dancer.getFirstName());
        dancerName.setFont(new Font("Dialog",Font.PLAIN,12));
        JLabel lastName = new JLabel("Last Name");
        JLabel dancerLastName = new JLabel(dancer.getLastName());
        dancerLastName.setFont(new Font("Dialog",Font.PLAIN,12));
        JLabel age = new JLabel("Age");
        JLabel dancerAge = new JLabel(String.valueOf(dancer.getAge()));
        dancerAge.setFont(new Font("Dialog",Font.PLAIN,12));
        JLabel country = new JLabel("Country");
        JLabel dancerCountry = new JLabel(dancer.getCountry());
        dancerCountry.setFont(new Font("Dialog",Font.PLAIN,12));
        JLabel info = new JLabel("Info");

        try {
            BufferedImage bufferedImage = ImageIO.read(new File("src/main/resources/" + dancer.getImage()));
            ImageIcon initialImage = new ImageIcon(bufferedImage);
            Image actual = initialImage.getImage();
            ImageIcon result = new ImageIcon();
            result.setImage(getScaledImage(actual, (WIDTH_APP*2)/3-50, HEIGHT_APP-50));
            panelRight.add(new JLabel(result),gridBagConstrains);
        }catch (IOException e)
        {
            return;
        }

        JTextArea dancerInfo = new JTextArea(10,10);
        dancerInfo.setText(dancer.getInfo());
        dancerInfo.setLineWrap(true);
        dancerInfo.setWrapStyleWord(true);
        dancerInfo.setFont(new Font("Dialog",Font.PLAIN,12));
        JScrollPane scroll = new JScrollPane(dancerInfo);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 0;
        panelLeft.add(firstName,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 1;
        panelLeft.add(dancerName,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 2;
        panelLeft.add(lastName,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 3;
        panelLeft.add(dancerLastName,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 4;
        panelLeft.add(age,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 5;
        panelLeft.add(dancerAge,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 6;
        panelLeft.add(country,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 7;
        panelLeft.add(dancerCountry,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 8;
        panelLeft.add(info,gridBagConstrains);

        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 9;
        panelLeft.add(scroll,gridBagConstrains);

        panelMain.add(panelLeft);
        panelMain.add(panelRight);

        frame.add(panelMain);
        frame.setVisible(true);
    }

    private Image getScaledImage(Image srcImg, int w, int h) {
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();

        return resizedImg;
    }

}
