package ui;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class AboutWDSFView extends JPanel {
    private JTextArea information = new JTextArea(
            "THE FEDERATION\n" +
            "The World DanceSport Federation - WDSF is the world governing body for DanceSport. It was founded 1957 under the \n" +
                    " name International Council of Amateur Dancers in Wiesbaden, Germany, and is now a non-governmental international\n" +
            "organisation constituted under Swiss law. It has its Headquarters at Maison du Sport Intrnational in Lausanne,\n Switzerland." +
            "The mission of WDSF is to regulate, administer and develop DanceSport. For the benefit of millions\n " +
                    "of athletes at every level and on all continents!\n" +
            "STATUTORY OBJECTS\n" +
            "As per its Statutes, WDSF pursues the following objectives."+
            "To advance, promote, and protect the character, status and interests of DanceSport worldwide.\n" +
            "To develop standardised rules governing international competitions.\n" +
            "To author and enforce Codes of Conduct and Standards of Ethics for both athletes and officials.\n" +
            "To advise and assist the WDSF National Member Bodies and the Associate Members in the administration of\n" +
                    " DanceSport in their countries and organisations.\n" +
            "To represent DanceSport in the Olympic Movement.\n" +
            "WDSF amends its governing documents regularly, reviewing the Statutes and complementing the Competition Rules\n" +
                    " with pertinent codes and regulations whenever the"+
            "need arises. The “Adjudicators’ Code of Conduct\n and Standards of Ethics” and the “WDSF Dress Regulations” are\n" +
                    "two of the most recent documents authored to meet" +
            "all challenges inherent in DanceSport.\n" +
            "WDSF also adapts its organisational structure to keep up with the principles of modern sports governance. The\n" +
                    " creation of the independent IDSF Disciplinary Council and"+
            "the Athletes Commission in 2006 and 2007,\n respectively, filled the remaining gaps in that respect." +
            "WDSF STRUCTURE\n" +
            "Democratic and efficient describe the style of governance and the structure of WDSF. The WDSF General Meeting\n" +
                    " elects the 15-member Presidium and delegates the\n" +
            "management to the latter. On the operational level, the Managing Committee – made up of the President, First Vice\n" +
                    " President, Treasurer, Secretary General and Sports\n" +
            "Director –  tends to the day-to-day running of the federation. The committee is supported in its task by the\n" +
                    " standing commissions, by advisers and consultants, and by the\n administration.\n" +
            "“VISION 2012” was an ambitious development plan for WDSF to redefine its mission and to serve an even broader\n" +
                    " constituent base. At the core of the vision is the holistic\n"+
            "view on DanceSport, an interpretation of DanceSport as the single and encompassing brand uniting all forms of dance.\n" +
            "2020 VISION is the new development plan of WDSF. It focuses on aligning the organisation as well as the sport with the\n" +
                    " objectives that are identified by members of a task force and by the WDSF constituents. The overriding goal of the 2020\n" +
                    "VISION process is to bring DanceSport to the next level and to make WDSF compliant with the highest"+
            "standards in\n governance.");
    public AboutWDSFView(){
        super(new GridBagLayout());
        information.setPreferredSize(new Dimension(650, 600));
        information.setBackground(new Color(85, 155, 132));
        GridBagConstraints gridBagConstrains = new GridBagConstraints();
        JPanel panelMain = new JPanel();
        panelMain.add(information,gridBagConstrains);
        this.add(panelMain);
    }
}
