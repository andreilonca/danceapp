package ui;

import db.DancersProvider;
import db.JDBCConnection;
import model.Dancer;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.List;


public class AthletesView extends JPanel {
    //private static final int WIDTH_APP = 100;
    //private static final int HEIGHT_APP = 100;

    public AthletesView() {
        super(new GridBagLayout());
        setBackground(new Color(85, 155, 132));

        String[] column = {"ID", "FirstName", "LastName", "Country", "Age"};
        final DancersProvider dancersProvider = new DancersProvider(JDBCConnection.getConnection());
        List<Dancer> dancerList = dancersProvider.provideDancers();
        final JTable jt = new JTable(convertToMatrix(dancerList), column);
        jt.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting() && jt.getSelectedRow() != -1) {
                    System.out.println(jt.getValueAt(jt.getSelectedRow(), 0).toString());
                    Object obj = jt.getValueAt(jt.getSelectedRow(), 0);

                    Dancer dancer = dancersProvider.getDancerByID(Integer.parseInt((String) obj));

                    new DancerView(dancer);
                }
            }
        });
        jt.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(jt);
        this.add(sp);
        //frame.add(sp);

        //frame.setVisible(true);
    }

    public String[][] convertToMatrix(List<Dancer> dancers) {
        String[][] result = new String[dancers.size()][5];
        for (int i = 0; i < dancers.size(); i++) {
            String[] line = new String[6];
            line[0] = String.valueOf(dancers.get(i).getId());
            line[1] = dancers.get(i).getFirstName();
            line[2] = dancers.get(i).getLastName();
            line[3] = dancers.get(i).getCountry();
            line[4] = String.valueOf(dancers.get(i).getAge());
            result[i] = line;
        }

        return result;
    }
}
