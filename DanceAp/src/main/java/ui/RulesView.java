package ui;

import javax.swing.*;
import java.awt.*;

public class RulesView extends JPanel {
    private JTextArea textArea = new JTextArea("GUIDE TO COMPETITION RULES\n" +
            "STRUCTURE\n" +
            "All Rules related to DanceSport Competitions can be found by navigating throught the menus on the left.\n" +
            "Rules for Adjudicators and Chairpersons that do not relate directly to competitions can be found in the Adjudicators'\n" +
            " Regulations section.\n" +
            "Rules for Athletes that do not relate directly to competitions can be found in the Athletes' Rules section.\n" +
            "Regulations for Competition Organizers can be found in the Organisers Regulations section.\n " +
            "ATHLETES' GUIDE TO RULES\n" +
            "STRUCTURE\n" +
            "The rules, regulations and codes that an athlete must be familiar with in order to compete in an WDSF granted competition\n" +
            "are included in this section. Summary information and all the pertinent documents \n" +
            "are provided under the respective menu items on the left.\n" +
            "COMPETITION\n" +
            "Includes WDSF Competition Rules, WDSF Dress Regulations, World Ranking Tournament regulations and additional\n" +
            " information of relevance to the athletes.   \n" +
            "ANTI-DOPING\n" +
            "Contains the IDSF Anti-Doping Code, provides extensive information on the WDSF anti-doping programme and\n" +
            " athlete-focused education through different World-Anti-Doping\n"+
            " Agency initiatives." + "\n"+
            "OFFICIAL GUIDE TO RULES\n" +
            "STRUCTURE\n" +
            "Officials serving DanceSport in whatever capacity are required to be thoroughly familiar with all the rules, regulations,\n" +
            " codes and policies that govern competitions. Additionally,they need to be knowledgeable about the norms and\n" +
            " standards applicable to their assigned duties, and about the requirements for their education and licensing. \n" +
            "Summary information and all the pertinent documents are provided under the respective menu items on the left.\n" +
            "OPERATING POLICIES\n" +
            "Contains the Operating Policies outlining the duties and obligations of WDSF Chairpersons, including the report\n" +
            " and checklist.\n" +
            "COMPETITIONS\n" +
            "Includes WDSF Competition Rules and all regulations, policies and protocols concerning the conduct of WDSF granted\n" +
            " competitions.\n" +
            "CONGRESSES\n" +
            "Contains congress registration forms for WDSF officials, bidding forms for congresses.");

    public RulesView() {
        super(new GridBagLayout());
        textArea.setPreferredSize(new Dimension(650, 600));
        textArea.setBackground(new Color(85, 155, 132));
        GridBagConstraints gridBagConstrains = new GridBagConstraints();
        JPanel panelMain = new JPanel();
        panelMain.add(textArea, gridBagConstrains);
        this.add(panelMain);
    }
}
