package ui;


import db.RefereesProvider;
import db.JDBCConnection;
import model.Referee;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class OfficialsView extends JPanel {

    public OfficialsView() {
        super(new GridBagLayout());
        setBackground(new Color(85, 155, 132));
        String[] column = {"ID", "FirstName", "LastName","Age","Country"};
        RefereesProvider refereesProvider = new RefereesProvider(JDBCConnection.getConnection());
        List<Referee> refereeList = refereesProvider.provideReferees();
        JTable jt = new JTable(convertToMatrix(refereeList), column);
        jt.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(jt);
        this.add(sp);
    }
    public String[][] convertToMatrix(List<Referee> referees) {
        String[][] result = new String[referees.size()][5];
        for (int i = 0; i < referees.size(); i++) {
            String[] line = new String[6];
            line[0] = String.valueOf(referees.get(i).getId());
            line[1] = referees.get(i).getFirstName();
            line[2] = referees.get(i).getLastName();
            line[3] = String.valueOf(referees.get(i).getAge());
            line[4] = referees.get(i).getCountry();

            result[i] = line;
        }

        return result;
    }
}
