package ui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class AboutDanceSportView extends JPanel{
    private JTextArea textArea = new JTextArea("WHEN DANCE BECAME SPORT\n" +
            " Early Days Dance turned into genuine sport at the beginning of the twentieth\n" +
            "century, when French entrepreneur Camille de Rhynal and a group of superb\n" +
            " dancers added the competitive to the social, and when they converted ballrooms\n" +
            " into the venue for their contests.\n" +
            "The first Tango tournament with international participation took place in Nice\n," +
            "France, in 1907. Ballroom championships in Paris, Berlin and London were soon\n" +
            " to follow. France, Germany and England continued to assume the lead in\n" +
            " fostering the emergence of a sport that seemed to fit in perfectly with the roaring\n" +
            " twenties.\n" +
            "An inaugural world championship truly deserving of such title was held in Bad\n" + "" +
            " Nauheim, Germany, in 1936. Couples from fifteen nations and three continents\n" +
            " were involved. Even though World War II then brought most of the competitive\n" +
            " dancing to a halt, a new sport was born.  One that succeeded to keep in step with\n" +
            " the rhythms of time.\n" +
            "DANCESPORT TODAY\n" +
            " Recent Days The World DanceSport Federation coined the term \"DanceSport\" in\n" +
            " the early 1980s. While the Sport in the composite aspires to be consistent with the\n" +
            " generally accepted definitions, Dance is to remain the distinguishing artistic mark.\n" +
            "For some of the styles, DanceSport takes pride in upholding some of the traditions\n" +
            " and panache of what was previously known as competitive ballroom dancing. But\n" +
            " it has long abandoned the latter's narrow confines. Today, the most diverse dance\n" +
            " styles that have adopted a sports-based culture, and that have established bona\n" +
            " fide competition structures, fall under the genus name.\n" +
            "DanceSport has become an all-encompassing brand for an activity that is uniquely\n" +
            " accessible and sociable, allowing participants to improve physical fitness and\n" +
            " mental well-being, to interact, and to obtain results at all levels. Everybody is\n" +
            " capable of moving to music. And dance transgresses all barriers of age, gender\n" +
            " and culture.");

    public AboutDanceSportView() {
        super(new GridBagLayout());
        GridBagConstraints gridBagConstrains = new GridBagConstraints();
        JPanel picturesPanel = new JPanel();
        picturesPanel.setLayout(new BorderLayout());
        picturesPanel.setPreferredSize(new Dimension(220, 620));
        picturesPanel.setBackground(new Color(85, 155, 132));
        JPanel infoPanel = new JPanel();
        try {
            BufferedImage balroomImage = ImageIO.read(new File("src/main/resources/balroom.jpg"));
            BufferedImage classicImage = ImageIO.read(new File("src/main/resources/classic.jpg"));

            ImageIcon balroomIcon = new ImageIcon(balroomImage);
            balroomIcon.setImage(getScaledImage(balroomImage, 200,250));
            ImageIcon classicIcon = new ImageIcon(classicImage);
            classicIcon.setImage(getScaledImage(classicImage, 200,250));
            //gridBagConstrains.gridx=0;
           // gridBagConstrains.gridy=0;
            picturesPanel.add(new JLabel(balroomIcon), BorderLayout.NORTH);
            //gridBagConstrains.gridx=0;
            //gridBagConstrains.gridy=1;
            picturesPanel.add(new JLabel(classicIcon), BorderLayout.SOUTH);


        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        textArea.setBackground(new Color(85, 155, 132));
        textArea.setPreferredSize(new Dimension(450, 620));
        gridBagConstrains.anchor = GridBagConstraints.EAST;
        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 0;
        infoPanel.add(textArea, gridBagConstrains);

        gridBagConstrains.gridx = 1;
        gridBagConstrains.gridy = 0;
        infoPanel.add(picturesPanel, gridBagConstrains);

        this.add(infoPanel);
        this.add(picturesPanel);

    }

    private Image getScaledImage(Image srcImg, int w, int h) {
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();

        return resizedImg;
    }
}
