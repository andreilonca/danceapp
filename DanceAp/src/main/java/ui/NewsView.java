package ui;
import db.JDBCConnection;
import db.NewsProvider;
import model.News;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;


public class NewsView extends JPanel {
    private NewsProvider newsProvider = new NewsProvider(JDBCConnection.getConnection());
    List<News> newsList = newsProvider.provideNews();
    int nrClick=0;
    public NewsView() {
        super(new GridBagLayout());
        setBackground(new Color(85, 155, 132));
        add(new TestPane());
    }

    public class TestPane extends JPanel {

        private JPanel mainList;

        public TestPane() {
            setLayout(new BorderLayout());

            mainList = new JPanel(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            gbc.weightx = 1;
            gbc.weighty = 1;
            mainList.add(new JPanel(), gbc);

            add(new JScrollPane(mainList));

            JButton add = new JButton("Read more");
            add.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(nrClick>=newsList.size()){
                        return;
                    }
                    JPanel newsElement = new JPanel();
                    if(nrClick%2==0) {
                        newsElement.setBackground(new Color(30, 155, 13));
                    } else {
                        newsElement.setBackground(new Color(11, 144, 155));
                    }
                    GridBagConstraints newsConstraints = new GridBagConstraints();
                    newsConstraints.gridx = 0;
                    newsConstraints.gridy = 0;
                    JTextArea jTextArea = new JTextArea();
                    jTextArea.setBackground(newsElement.getBackground());
                    jTextArea.setBounds(0, 0, 300, 300);
                    jTextArea.setText(newsList.get(nrClick).getText());
                    jTextArea.setLineWrap(true);
                    jTextArea.setWrapStyleWord(true);
                    newsElement.add(jTextArea, newsConstraints);
                    try {
                        BufferedImage bufferedImage = ImageIO.read(new File("src/main/resources/"+ newsList.get(nrClick).getImage()));
                        ImageIcon initialImage = new ImageIcon(bufferedImage);
                        Image actual = initialImage.getImage();
                        ImageIcon result = new ImageIcon();
                        result.setImage(getScaledImage(actual, 300,100));
                        newsConstraints.gridy = 1;
                        newsElement.add(new JLabel(result), newsConstraints);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    GridBagConstraints gbc = new GridBagConstraints();
                    gbc.gridwidth = GridBagConstraints.REMAINDER;
                    gbc.weightx = 1;
                    gbc.fill = GridBagConstraints.HORIZONTAL;

                    mainList.add(newsElement, gbc, 0);

                    validate();
                    repaint();
                    nrClick++;
                }
            });

            add(add, BorderLayout.SOUTH);

        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(500, 500);
        }

        private Image getScaledImage(Image srcImg, int w, int h) {
            BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = resizedImg.createGraphics();

            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.drawImage(srcImg, 0, 0, w, h, null);
            g2.dispose();

            return resizedImg;
        }
    }


}
