package ui;

import com.sun.jmx.remote.security.JMXPluggableAuthenticator;
import listeners.ItemSelectedListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AppView extends JFrame implements ItemSelectedListener {
    private static final long serialVersionUID = 1L;

    private static final String EVENT_CALENDAR = "eventCalendar";
    private static final String ATHLETES = "athletes";
    private static final String OFFICIALS = "officials";
    private static final String NEWS = "news";
    private static final String ABOUT_DANCE = "aboutDance";
    private static final String ABOUT_WDSF = "aboutWDSF";
    private static final String RULES = "rules";

    private JButton eventCalendar = new JButton("Event Calendar");
    private JButton athletes = new JButton("Athletes");
    private JButton oficials = new JButton("Officials");
    private JButton news = new JButton("News");
    private JButton aboutDanceSport = new JButton("About Dance Sport");
    private JButton aboutWDSF = new JButton("About WDSF");
    private JButton rules = new JButton("Rules");
    private static final int TWENTY = 20;
    private static final int WIDTH_APP = 1000;
    private static final int HEIGHT_APP = 620;
    private static final int INSESTS_VALUES = 5;

    JFrame frame = new JFrame();
    JPanel panelMain = new JPanel();
    JPanel panelRight;

    public AppView() {
        frame.setSize(WIDTH_APP+20, HEIGHT_APP);
        frame.setTitle("Dance App");
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        GridBagConstraints gridBagConstrains = new GridBagConstraints();
        JPanel panelLeft = new JPanel(new GridBagLayout());
        panelLeft.setBackground(new Color(127, 72, 155));
        panelLeft.setPreferredSize(new Dimension(WIDTH_APP/3,HEIGHT_APP));
        panelRight = new JPanel(new CardLayout());
        panelRight.setBackground(new Color(85, 155, 132));
        panelRight.setPreferredSize(new Dimension((WIDTH_APP*2)/3, HEIGHT_APP));
        addComponentToPane();
        //panelRight.add(new AthletesView());

        //panelRight.add(ex1, gridBagConstrains);
        //panelRight.add(ex, gridBagConstrains);
        //ex1.setPreferredSize(new Dimension(100, 100));
        //ex.setPreferredSize(new Dimension(400, 400));
        panelMain.add(panelLeft);
        panelMain.add(panelRight);

        gridBagConstrains.insets = new Insets(INSESTS_VALUES, INSESTS_VALUES, INSESTS_VALUES, INSESTS_VALUES);
        setupLabels(gridBagConstrains, panelLeft);
        frame.add(panelMain);

        //set listeners
        setEventCalendarListener();
        setAthletesListener();
        setOficialListener();
        setNewsListener();
        setAboutDanceSportListener();
        setAboutWDSFListener();
        setRulesListener();

        frame.setVisible(true);

    }

    private void setupLabels(GridBagConstraints gridBagConstrains, JPanel panelForm) {

        gridBagConstrains.anchor = GridBagConstraints.EAST;
        gridBagConstrains.gridx = 0;        //columns
        gridBagConstrains.gridy = 0;        //rows
        panelForm.add(eventCalendar, gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(athletes, gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(oficials, gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(news, gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(aboutDanceSport, gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(aboutWDSF, gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(rules, gridBagConstrains);
        gridBagConstrains.gridy++;
    }

    private void setEventCalendarListener() {
        eventCalendar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //ex.setText("Event Calendar clicked");
               //showScreen(new EventCalendarView());
                onItemClicked(EVENT_CALENDAR);
            }
        });

    }

    private void setAthletesListener() {
        athletes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //showScreen(new AthletesView());
                onItemClicked(ATHLETES);
            }
        });
    }

    private void setOficialListener() {
        oficials.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //ex.setText("Oficials clicked");
                //showScreen(new OfficialsView());
                onItemClicked(OFFICIALS);
            }
        });
    }

    private void setNewsListener() {
        news.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               // ex.setText("News clicked");
               //showScreen(new NewsView());
                onItemClicked(NEWS);
            }
        });
    }

    private void setAboutDanceSportListener(){
        aboutDanceSport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //ex.setText("About Dance Sport clicked");
                //showScreen(new AboutDanceSportView());
                onItemClicked(ABOUT_DANCE);
            }
        });
    }
    private void setAboutWDSFListener(){
        aboutWDSF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //ex.setText("About WDSF clicked");
                //showScreen(new AboutWDSFView());
                onItemClicked(ABOUT_WDSF);
            }
        });
    }
    private void setRulesListener(){
        rules.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //ex.setText("Rules clicked");
                //showScreen(new RulesView());
                onItemClicked(RULES);
            }
        });
    }

   /* private void showScreen(JPanel screen) {
        frame.getContentPane().remove(panelRight);
        panelRight = screen;
        panelRight.setBackground(new Color(155, 14, 36));
        panelRight.setPreferredSize(new Dimension((WIDTH_APP*2)/3, HEIGHT_APP));
        frame.add(panelRight);
        frame.validate();
        panelRight.repaint();
    }*/

    public void addComponentToPane(){
        JPanel eventCalendarPanel = new EventCalendarView();
        panelRight.add(eventCalendarPanel,EVENT_CALENDAR);

        JPanel athletesePanel = new AthletesView();
        panelRight.add(athletesePanel,ATHLETES);

        JPanel officialsPanel = new OfficialsView();
        panelRight.add(officialsPanel,OFFICIALS);

        JPanel newsPanel = new NewsView();
        panelRight.add(newsPanel,NEWS);

        JPanel aboutDancePanel = new AboutDanceSportView();
        panelRight.add(aboutDancePanel,ABOUT_DANCE);

        JPanel aboutWDSFPanel = new AboutWDSFView();
        panelRight.add(aboutWDSFPanel,ABOUT_WDSF);

        JPanel rulesPanel = new RulesView();
        panelRight.add(rulesPanel,RULES);
    }


    @Override
    public void onItemClicked(String itemId) {
        CardLayout cl = (CardLayout)(panelRight.getLayout());
        cl.show(panelRight,itemId);
    }
}

