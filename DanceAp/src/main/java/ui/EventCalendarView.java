package ui;

import db.EventsProvider;
import db.JDBCConnection;
import model.Event;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class EventCalendarView extends JPanel {


    public EventCalendarView() {
        super(new BorderLayout());
        String[] column = {"ID", "Name", "Location", "Country", "Date"};
        EventsProvider eventsProvider = new EventsProvider(JDBCConnection.getConnection());
        List<Event> eventList = eventsProvider.provideEvents();
        JTable jt = new JTable(convertToMatrix(eventList), column);
        jt.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(jt);
        this.add(sp);
        JPanel buttonPanel = new JPanel();
        JButton addButton = new JButton("Add new event");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new InsertEventsView();
            }
        });
        buttonPanel.add(addButton);
        this.add(buttonPanel,BorderLayout.SOUTH);
       
    }


    public String[][] convertToMatrix(List<Event> events) {
        String[][] result = new String[events.size()][5];
        for (int i = 0; i < events.size(); i++) {
            String[] line = new String[5];
            line[0] = String.valueOf(events.get(i).getId());
            line[1] = events.get(i).getName();
            line[2] = events.get(i).getLocation();
            line[3] = events.get(i).getCountry();
            line[4] = events.get(i).getDate();
            result[i] = line;
        }

        return result;
    }
}
