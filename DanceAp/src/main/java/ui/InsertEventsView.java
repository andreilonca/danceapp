package ui;

import db.EventsProvider;
import db.JDBCConnection;
import model.Event;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class InsertEventsView extends JFrame {
    private static final int WIDTH_APP = 300;
    private static final int HEIGHT_APP = 400;
    JLabel nameLabel = new JLabel("Name");
    JLabel locationLabel = new JLabel("Location");
    JLabel countryLabel = new JLabel("country");
    JLabel dateLabel = new JLabel("date");
    JTextField nameTextField = new JTextField(10);
    JTextField locationTextField = new JTextField(10);
    JTextField countryTextField = new JTextField(10);
    JTextField dateTextField = new JTextField(10);
    JButton addButton =new JButton("Add");
    public InsertEventsView() {
        setBackground(new Color(85, 155, 132));
        //JPanel frame = new JPanel();
        this.setLayout(new GridBagLayout());
        this.setSize(WIDTH_APP, HEIGHT_APP);
        this.setTitle("Insert Event");
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
               System.out.println("Fereastra s-a inchis");
                e.getWindow().dispose();
            }
        });*/
        nameLabel.setPreferredSize(new Dimension(100,50));
        locationLabel.setPreferredSize(new Dimension(100,50));
        countryLabel.setPreferredSize(new Dimension(100,50));
        dateLabel.setPreferredSize(new Dimension(100,50));
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Event event = new Event();
                event.setName(nameTextField.getText());
                event.setLocation(locationTextField.getText());
                event.setCountry(countryTextField.getText());
                event.setDate(dateTextField.getText());
                EventsProvider eventsProvider = new EventsProvider(JDBCConnection.getConnection());
                eventsProvider.insertEvent(event);
                InsertEventsView.this.dispatchEvent(new WindowEvent(InsertEventsView.this,WindowEvent.WINDOW_CLOSING));
            }
        });
        /*nameTextField.setPreferredSize(new Dimension(100,50));
        locationTextField.setPreferredSize(new Dimension(100,50));
        countryTextField.setPreferredSize(new Dimension(100,50));
        dateTextField.setPreferredSize(new Dimension(100,50));
        addButton.setPreferredSize(new Dimension(200,50));*/
        GridBagConstraints gridBagConstrains = new GridBagConstraints();
        gridBagConstrains.anchor = GridBagConstraints.EAST;
        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 0;
        this.add(nameLabel, gridBagConstrains);
        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 1;
        this.add(locationLabel, gridBagConstrains);
        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 2;
        this.add(countryLabel, gridBagConstrains);
        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 3;
        this.add(dateLabel, gridBagConstrains);
        gridBagConstrains.gridx = 1;
        gridBagConstrains.gridy = 0;
        this.add(nameTextField, gridBagConstrains);
        gridBagConstrains.gridx = 1;
        gridBagConstrains.gridy = 1;
        this.add(locationTextField, gridBagConstrains);
        gridBagConstrains.gridx = 1;
        gridBagConstrains.gridy = 2;
        this.add(countryTextField, gridBagConstrains);
        gridBagConstrains.gridx = 1;
        gridBagConstrains.gridy = 3;
        this.add(dateTextField, gridBagConstrains);
        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy = 4;
        this.add(addButton, gridBagConstrains);
        this.setVisible(true);
    }
}
