package model;

public class News {
    int id;
    String text;
    String image;
    public News(){

    }
    public News(int id,String text,String image){
        this.id =id;
        this.text=text;
        this.image=image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
