package model;

public class Couple {
    private int id;
    private int id_male;
    private int id_female;

    public Couple() {

    }

    public Couple(int id, int id_male, int id_female) {
        this.id = id;
        this.id_male = id_male;
        this.id_female = id_female;
    }

    public int getId() {
        return id;
    }

    public int getId_male() {
        return id_male;
    }

    public int getId_female() {
        return id_female;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_male(int id_male) {
        this.id_male = id_male;
    }

    public void setId_female(int id_female) {
        this.id_female = id_female;
    }

    @Override
    public String toString() {
        return "Couple{" +
                "id=" + id +
                ", id_male=" + id_male +
                ", id_female=" + id_female +
                '}';
    }
}

