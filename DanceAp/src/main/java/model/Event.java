package model;

/**
 * Created with IntelliJ IDEA.
 * User: Andrei Lonca
 * Date: 20.04.2018
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */
public class Event {
    int id;
    String name;
    String location;
    String country;
    String date;

    public Event() {

    }

    public Event(int id, String name, String location, String country, String date) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.country = country;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getCountry() {
        return country;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", country='" + country + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
